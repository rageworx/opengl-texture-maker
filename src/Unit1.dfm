object MForm: TMForm
  Left = 190
  Top = 104
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  ClientHeight = 338
  ClientWidth = 266
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object LabelCopyright: TLabel
    Left = 0
    Top = 325
    Width = 266
    Height = 13
    Align = alBottom
    Alignment = taRightJustify
    BiDiMode = bdLeftToRight
    Caption = 'Copyright'
    Enabled = False
    ParentBiDiMode = False
  end
  object ProgressBar: TGauge
    Left = 90
    Top = 88
    Width = 169
    Height = 17
    ForeColor = clMenuBar
    Progress = 0
  end
  object Label1: TLabel
    Left = 96
    Top = 56
    Width = 56
    Height = 13
    Caption = 'Data Type :'
    Enabled = False
  end
  object BtnLoadImage: TButton
    Left = 181
    Top = 0
    Width = 81
    Height = 25
    Caption = 'Loading Image'
    TabOrder = 0
    OnClick = BtnLoadImageClick
  end
  object BtnConvert: TButton
    Left = 5
    Top = 56
    Width = 81
    Height = 49
    Caption = 'Converting2C'
    Enabled = False
    TabOrder = 1
    OnClick = BtnConvertClick
  end
  object LEDIT_ValName: TLabeledEdit
    Left = 8
    Top = 28
    Width = 257
    Height = 21
    AutoSize = False
    EditLabel.Width = 73
    EditLabel.Height = 13
    EditLabel.Caption = 'Method Name :'
    Enabled = False
    ImeName = #54620#44397#50612' '#51077#47141' '#49884#49828#53596' (IME 2000)'
    LabelPosition = lpAbove
    LabelSpacing = 3
    TabOrder = 2
  end
  object BtnSave: TButton
    Left = 0
    Top = 248
    Width = 265
    Height = 25
    Caption = 'Save to compatible C/C++ code file  ...'
    Enabled = False
    TabOrder = 3
    OnClick = BtnSaveClick
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 112
    Width = 265
    Height = 129
    Caption = 'LOG'
    TabOrder = 5
    object EditLog: TRichEdit
      Left = 8
      Top = 16
      Width = 249
      Height = 105
      Font.Charset = HANGEUL_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ImeName = #54620#44397#50612' '#51077#47141' '#49884#49828#53596' (IME 2000)'
      ParentFont = False
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object ComboType: TComboBox
    Left = 160
    Top = 56
    Width = 105
    Height = 21
    Style = csDropDownList
    Enabled = False
    ImeName = #54620#44397#50612' '#51077#47141' '#49884#49828#53596' (IME 2000)'
    ItemHeight = 13
    ItemIndex = 4
    TabOrder = 6
    Text = 'RGB888'
    Items.Strings = (
      'ARGB8888'
      'ARGB4444'
      'RGB565'
      'RGB555'
      'RGB888')
  end
  object CheckAlphaPicture: TCheckBox
    Left = 96
    Top = 3
    Width = 81
    Height = 17
    Caption = 'Merge Alpha'
    Checked = True
    Enabled = False
    State = cbChecked
    TabOrder = 7
  end
  object BtnSaveToBin: TButton
    Left = 0
    Top = 272
    Width = 265
    Height = 25
    Caption = 'Save to non-head-binary file ...'
    Enabled = False
    TabOrder = 8
    OnClick = BtnSaveToBinClick
  end
  object BtnCopy2Clipboard: TButton
    Left = 0
    Top = 296
    Width = 265
    Height = 25
    Caption = 'Copy to Clipboard'
    Enabled = False
    TabOrder = 4
    OnClick = BtnCopy2ClipboardClick
  end
  object OpenPictureDialog: TOpenPictureDialog
    Filter = 'Windosw Bitmap (*bmp)|*bmp'
    Title = 'Select Bitmap Image'
    Left = 96
    Top = 152
  end
  object SaveDialog: TSaveDialog
    Filter = 'C file|*.c|CPP file|*.cpp'
    Title = 'Save as'
    Left = 128
    Top = 152
  end
end
