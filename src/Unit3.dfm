object LicenseForm: TLicenseForm
  Left = 741
  Top = 103
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Licensing'
  ClientHeight = 149
  ClientWidth = 175
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 0
    Width = 159
    Height = 13
    Caption = 'Enter a license key for registering.'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Bevel1: TBevel
    Left = 8
    Top = 72
    Width = 161
    Height = 2
  end
  object LabeledEdit1: TLabeledEdit
    Left = 64
    Top = 24
    Width = 105
    Height = 21
    EditLabel.Width = 53
    EditLabel.Height = 13
    EditLabel.Caption = 'UserName:'
    ImeName = #54620#44397#50612' '#51077#47141' '#49884#49828#53596' (IME 2000)'
    LabelPosition = lpLeft
    TabOrder = 0
  end
  object LabeledEdit2: TLabeledEdit
    Left = 64
    Top = 48
    Width = 105
    Height = 21
    EditLabel.Width = 47
    EditLabel.Height = 13
    EditLabel.Caption = 'Company:'
    ImeName = #54620#44397#50612' '#51077#47141' '#49884#49828#53596' (IME 2000)'
    LabelPosition = lpLeft
    TabOrder = 1
  end
  object LabeledEdit3: TLabeledEdit
    Left = 8
    Top = 96
    Width = 161
    Height = 21
    EditLabel.Width = 64
    EditLabel.Height = 13
    EditLabel.Caption = 'License Key :'
    ImeName = #54620#44397#50612' '#51077#47141' '#49884#49828#53596' (IME 2000)'
    TabOrder = 2
  end
  object Button1: TButton
    Left = 8
    Top = 120
    Width = 161
    Height = 25
    Caption = 'OK.'
    TabOrder = 3
  end
end
