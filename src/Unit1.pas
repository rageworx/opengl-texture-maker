unit Unit1;
{ *****************************************************************************
  * Texture C code converter , version 0.2
  * =======================================
  * (C)2003~2005
  *****************************************************************************}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls, ExtDlgs, Clipbrd, Gauges;

type
  TImageRec = record
    Width     : integer;
    Height    : integer;
    BPP       : byte;
  end; // of TImageRec

  TMForm = class(TForm)
    BtnLoadImage: TButton;
    BtnConvert: TButton;
    LEDIT_ValName: TLabeledEdit;
    BtnSave: TButton;
    BtnCopy2Clipboard: TButton;
    OpenPictureDialog: TOpenPictureDialog;
    GroupBox1: TGroupBox;
    EditLog: TRichEdit;
    SaveDialog: TSaveDialog;
    LabelCopyright: TLabel;
    ProgressBar: TGauge;
    ComboType: TComboBox;
    Label1: TLabel;
    CheckAlphaPicture: TCheckBox;
    BtnSaveToBin: TButton;
    procedure FormCreate(Sender: TObject);
    procedure BtnLoadImageClick(Sender: TObject);
    procedure BtnConvertClick(Sender: TObject);
    procedure BtnCopy2ClipboardClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtnSaveToBinClick(Sender: TObject);
  private
    { Private declarations }
    ConvertedData       : array of char;
    ConvertedDataCount  : dword;
    IsConverted         : boolean;
    ImageInfo           : TImageRec;
    FirstRun            : boolean;

    function  convertWBGR2ARGB8888(WBGR : dword):dword;
    function  convertARGB8888ARGB4444(ARGB8888 : dword):word;
    function  convertARGB8888RGB565(ARGB8888 : dword):word;
    function  convertARGB8888RGB555(ARGB8888 : dword):word;
    procedure convertRGB2Float(RGB:dword;var RF,GF,BF:Extended);
    procedure convertRGB2byte(RGB:dword;var R,G,B:byte);
    procedure DisplayImageInfo(Image: TBitmap);
    procedure ImportMethodNameFromFileName(filename : string);
    procedure ReportLog(LogString:string);
    procedure ConvertBMPtoArray;
    function  ConvertString(str : string) : string;

    procedure ClearConvertedData;
    procedure Add2ConvertedData(str:string);
  public
    { Public declarations }
  end;

var
  MForm: TMForm;

implementation

uses Unit2;

{$R *.dfm}

const
  DEF_APP_CLASS_NAME  = 'OGLTM';
  DEF_APP_NAME        = 'OpenGL_TMaker';
  DEF_APP_VERSION     = 'v 0.6.0.en';
  DEF_APP_COPYRIGHT   = '(C)2003-2007 Rage Freeware';
  DEF_APP_AUTHOR      = 'M.C.Kim(rage.kim@gmail.com)';
  DEF_CRLF            = #13+#10;

  DEF_MASK_RED        = $000000FF;
  DEF_MASK_GREEN      = $0000FF00;
  DEF_MASK_BLUE       = $00FF0000;
  DEF_BIT_GREEN       = 8;
  DEF_BIT_BLUE        = 16;

  DEF_MAX_RES         = 1024;

  DEF_DT_ARGB8888     = 0;
  DEF_DT_ARGB4444     = 1;
  DEF_DT_RGB565       = 2;
  DEF_DT_RGB555       = 3;
  DEF_DT_RGB888       = 4;

  DEF_TXT_HEADER = '/***********************************************************'+ DEF_CRLF +
                   '** '+DEF_APP_NAME + ' ' + DEF_APP_VERSION + DEF_CRLF +
                   '** '+DEF_APP_COPYRIGHT + ' ' + DEF_APP_AUTHOR + DEF_CRLF +
                   '***********************************************************/' + DEF_CRLF;

  DEF_LIC_ENDE_KEY    = $0E18;
  DEF_LIC_START_STR   = 'OGLESTM';
  DEF_LIC_END_STR     = 'A';

procedure TMForm.FormCreate(Sender: TObject);
begin
  Application.Title       := DEF_APP_CLASS_NAME;
  MForm.Caption           := DEF_APP_NAME + ' ' + DEF_APP_VERSION;
  LabelCopyright.Caption  := DEF_APP_AUTHOR;

  MForm.DoubleBuffered    := True;
  IsConverted             := False;
  FirstRun                := True;
end;


procedure TMForm.convertRGB2Float(RGB:dword;var RF,GF,BF:Extended);
var
  intR,
  intG,
  intB   : byte;
  ratioR,
  ratioG,
  ratioB : Extended;
begin
  intR := RGB AND DEF_MASK_RED;
  intG := RGB AND DEF_MASK_GREEN SHR DEF_BIT_GREEN;
  intB := RGB AND DEF_MASK_BLUE  SHR DEF_BIT_BLUE;

  ratioR := (100 * intR) / 256;
  ratioG := (100 * intG) / 256;
  ratioB := (100 * intB) / 256;

  RF := ratioR;
  GF := ratioG;
  BF := ratioB;
end;

procedure TMForm.convertRGB2byte(RGB:dword;var R,G,B:byte);
begin
  R := (RGB AND DEF_MASK_RED);
  G := (RGB AND DEF_MASK_GREEN) SHR DEF_BIT_GREEN;
  B := (RGB AND DEF_MASK_BLUE ) SHR DEF_BIT_BLUE;
end;

procedure TMForm.DisplayImageInfo(Image: TBitmap);
begin
  EditLog.Lines.Clear;
  EditLog.Lines.Add('resolution = '+IntToStr(Image.Width)+'x'+IntToStr(Image.Height));
end;

procedure TMForm.BtnLoadImageClick(Sender: TObject);
var
  MoveX,
  MoveY : integer;
begin
  if (OpenPictureDialog.Execute) then
  begin
    if (FileExists(OpenPictureDialog.FileName)) then
    begin
      TextureForm.TextureImage.Picture.LoadFromFile(OpenPictureDialog.FileName);
      MoveX := MForm.Left;
      MoveY := MForm.Top;
      while not((MoveX <= 0) and (MoveY <= 0)) do
      begin
        dec(MoveX,20);
        dec(MoveY,20);
        if (MoveX < 0) then MoveX := 0;
        if (MoveY < 0) then MoveY := 0;

        MForm.Left := MoveX;
        MForm.Top  := MoveY;

        MForm.Repaint;
      end;
      if (MForm.Left < 0) then MForm.Left := 0;
      if (MForm.Top  < 0) then MForm.Top  := 0;
      TextureForm.Top  := MForm.Top;
      TextureForm.Left := MForm.Left + MForm.Width;
      ImageInfo.Width  := TextureForm.TextureImage.Picture.Width;
      ImageInfo.Height := TextureForm.TextureImage.Picture.Height;
      ImageInfo.BPP    := 32;
      if (ImageInfo.Width <> ImageInfo.Height) then
      begin
        MessageBox(0,'It may not works caused difference of texture width and height','warnning!',0);
        IsConverted := False;
        ReportLog('Warnning::Diff.Image.Size');
      end;
      if ((ImageInfo.Width mod 8)<>0) then
      begin
        MessageBox(0,'It must be mutiply of 8th , texture width and height','error!',0);
        IsConverted := False;
        ReportLog('Fail::Diff.Image.Size');
        Exit;
      end;
      if (ImageInfo.Width > DEF_MAX_RES) OR (ImageInfo.Height > DEF_MAX_RES) then
      begin
        MessageBox(0,'It overed maximum size of texture width or height !','error!',0);
        IsConverted := False;
        ReportLog('Fail::WrongSize');
        Exit;
      end;
      TextureForm.Left                := MForm.Left + MForm.Width;
      TextureForm.Top                 := MForm.Top;
      TextureForm.TextureImage.Width  := ImageInfo.Width;
      TextureForm.TextureImage.Height := ImageInfo.Height;
      TextureForm.SetResizeFormToTexureImage;

      DisplayImageInfo(TextureForm.TextureImage.Picture.Bitmap);
      ImportMethodNameFromFileName(OpenPictureDialog.FileName);
      ReportLog('Success::Load-BMP');
      ReportLog(ExtractFileName(OpenPictureDialog.FileName));

      BtnConvert.Enabled        := True;
      LEDIT_ValName.Enabled     := True;
      BtnConvert.Enabled        := True;
      BtnSave.Enabled           := False;
      BtnSaveToBin.Enabled      := True;
      BtnCopy2Clipboard.Enabled := False;

      TextureForm.Show;
    end;
  end;
end;

procedure TMForm.ImportMethodNameFromFileName(filename : string);
var
  return_string  : string;
  extention_size : byte;
begin
  return_string  := '';
  return_string  := ExtractFileName(filename);
  extention_size := length(ExtractFileExt(filename));
  setlength(return_string, length(return_string) - extention_size);
  LEDIT_ValName.Text := ConvertString(return_string);
end;

procedure TMForm.ReportLog(LogString:string);
begin
  EditLog.Lines.Add(LogString);
  EditLog.Repaint;
end;

procedure TMForm.ClearConvertedData;
begin
  //SetLength(ConvertedData,0);
  ConvertedDataCount := 0;
end;

procedure TMForm.Add2ConvertedData(str:string);
var
  cnt : dword;
begin
  for cnt:=1 to Length(str) do
  begin
    ConvertedData[ConvertedDataCount] := str[cnt];
    inc(ConvertedDataCount);
  end;

end;

function  TMForm.convertWBGR2ARGB8888(WBGR : dword):dword;
var
  rtn_val : dword;
begin
  rtn_val := (( WBGR AND $FF000000)) OR
             (( WBGR AND $000000FF) SHL 16) OR
             (( WBGR AND $0000FF00)) OR
             (( WBGR AND $00FF0000) SHR 16);
  result  := rtn_val;
end;

function  TMForm.convertARGB8888ARGB4444(ARGB8888 : dword):word;
var
  rtn_val : word;
begin
  rtn_val := (( ARGB8888 AND $F0000000 ) SHR 20) OR
             (( ARGB8888 AND $00F00000 ) SHR 16) OR
             (( ARGB8888 AND $0000F000 ) SHR  8) OR
             (( ARGB8888 AND $000000F0 ) SHR  4);
  result  := rtn_val;
end;

function  TMForm.convertARGB8888RGB565(ARGB8888 : dword):word;
var
  rtn_val : word;
begin
  rtn_val := (( ARGB8888 AND $00F80000 ) SHR 16) OR
             (( ARGB8888 AND $00007E00 ) SHR  4) OR
             (( ARGB8888 AND $0000001E ));
  result  := rtn_val;
end;

function  TMForm.convertARGB8888RGB555(ARGB8888 : dword):word;
var
  rtn_val : word;
begin
  rtn_val := (( ARGB8888 AND $00F80000 ) SHR 16) OR
             (( ARGB8888 AND $00007C00 ) SHR  4) OR   // Fixed.
             (( ARGB8888 AND $0000001E ));
  result  := rtn_val;
end;

procedure TMForm.ConvertBMPtoArray;
var
  cntX , cntY : integer;
  tmpRGB      : dword;
  tmpR,
  tmpG,
  tmpB        : byte;
  tmpStr      : string;
  fncName     : string;
  count,
  max         : dword;
  preTick,
  nowTick     : dword;
begin
  // Fast Cache Instruct for more speedable converting
  ReportLog('Allocating memory ....');
  ClearConvertedData;

  count := (Length(DEF_TXT_HEADER)) + (ImageInfo.Width * ImageInfo.Height * 17) + 255;

  SetLength(ConvertedData,count);
  
  sleep(1);
  ReportLog('Alloc = '+IntToStr(Length(ConvertedData) div 1024)+'Kbytes');
  sleep(1);

  fncName := LEDIT_ValName.Text;

  preTick := GetTickCount;
  ReportLog('Start::Convert');

  Add2ConvertedData(DEF_TXT_HEADER);
  Add2ConvertedData(DEF_CRLF);
  Add2ConvertedData('/* Image Width  = ' + IntToStr(ImageInfo.Width) + ' */' + DEF_CRLF);
  Add2ConvertedData('/* Image Height = ' + IntToStr(ImageInfo.Height) + ' */' + DEF_CRLF);
  Add2ConvertedData('unsigned char ' + fncName + '[] = { '+ DEF_CRLF);

  max   := ImageInfo.Width * ImageInfo.Height;
  count := 0;
  ProgressBar.Progress := 0;

  for cntY:=0 to ImageInfo.Height-1 do
  for cntX:=0 to (ImageInfo.Width-1) do
  begin
    tmpRGB := TextureForm.TextureImage.Canvas.Pixels[cntX,cntY];

    if (ComboType.ItemIndex = DEF_DT_RGB888) then
    begin
      convertRGB2byte(tmpRGB,tmpR,tmpG,tmpB);
      tmpStr := Format('0x%x,0x%x,0x%x,',[tmpR,tmpG,tmpB]);
      Add2ConvertedData(tmpStr + DEF_CRLF);
    end
    else
    if (ComboType.ItemIndex = DEF_DT_ARGB8888 ) then
    begin
      convertRGB2byte(tmpRGB,tmpR,tmpG,tmpB);
      tmpStr := Format('0x%x,0x%x,0x%x,',[tmpR,tmpG,tmpB]);
      Add2ConvertedData(tmpStr + DEF_CRLF);
    end
    else
    if (ComboType.ItemIndex = DEF_DT_ARGB4444 ) then
    begin
      tmpStr := Format('0x%x',[]);
      Add2ConvertedData(tmpStr + DEF_CRLF);
    end
    else
    if (ComboType.ItemIndex = DEF_DT_RGB565 ) then
    begin
    end
    else
    begin
    end;

    Inc(count);
    ProgressBar.Progress := (100 * count) div max;
  end;

  ConvertedData[ConvertedDataCount-2] := ' ';

  Add2ConvertedData('};');

  SetLength(ConvertedData,ConvertedDataCount);

  nowTick := GetTickCount;

  ReportLog('End::Convert');
  ReportLog('Taken Time ='+IntToStr((nowTick - preTick) div 1000)+'seconds');
end;

procedure TMForm.BtnConvertClick(Sender: TObject);
begin
  BtnConvert.Enabled        := False;
  BtnSave.Enabled           := False;
  BtnSaveTobin.Enabled      := False;
  BtnCopy2Clipboard.Enabled := False;
  ReportLog('Now converting texture ...');
  ReportLog('Wait a moment ...');
  ConvertBMPtoArray;
  BtnConvert.Enabled        := True;
  BtnSave.Enabled           := True;
  BtnSaveToBin.Enabled      := True;
  BtnCopy2Clipboard.Enabled := True;
end;

procedure TMForm.BtnCopy2ClipboardClick(Sender: TObject);
var
  tmpClip : TClipBoard;
begin
  tmpClip := TClipBoard.Create;
  tmpClip.Clear;
  tmpClip.AsText := String(ConvertedData);
  tmpClip.Free;
end;

function  TMForm.ConvertString(str : string) : string;
var
  max,
  cnt    : word;
  rtnStr,
  tmpStr : string;
begin
  rtnStr := '';
  tmpStr := '';

  max    := Length(str);

  for cnt:=1 to max do
  begin
    if (Ord(str[cnt]) > $7F) then
    begin
      tmpStr := Format('%x',[Ord(str[cnt])]);
    end
    else
    if (str[cnt] = ' ') then
    begin
      tmpStr := '_';
    end
    else
    begin
      tmpStr := str[cnt];
    end;

    rtnStr := rtnStr + tmpStr;
  end;

  result := rtnStr;
end;

procedure TMForm.BtnSaveClick(Sender: TObject);
var
  f : textfile;
  wr : boolean;
begin
  wr := False;
  SaveDialog.FileName   := LEDIT_ValName.Text + '.c';
  SaveDialog.Filter     := 'C file|*.c|CPP file|*.cpp';
  SaveDialog.DefaultExt := 'c';
  SaveDialog.Title      := 'Save C file as ...';

  if (SaveDialog.Execute) then
  begin
    if FileExists(SaveDialog.FileName) then
    begin
      if IDYES = MessageBox(0,'Do you want to overwrite file ?','Exists File!',MB_YESNO) then wr := True
      else                                                                                    wr := False;
    end
    else wr := True;
  end;

  if (wr) then
  begin
    AssignFile(f,SaveDialog.FileName);
    {$I-}
    ReWrite(f);
    {$I+}
    if IOresult <> 0 then
    begin
      MessageBox(0,'Error currupted while writing file','error!',0);
      exit;
    end;

    Write(f,String(ConvertedData));

    Closefile(f);
  end;
end;

procedure TMForm.BtnSaveToBinClick(Sender: TObject);
var
  f       : file;
  wr      : boolean;
  cntX,
  cntY    : word;
  tmpRGB  : dword;
  tmpR,
  tmpG,
  tmpB    : byte;
begin
  wr := False;
  SaveDialog.FileName   := LEDIT_ValName.Text + '.tex';
  SaveDialog.Filter     := 'TEX file|*.tex';
  SaveDialog.DefaultExt := 'tex';
  SaveDialog.Title      := 'Save TEX file as ...';

  if (SaveDialog.Execute) then
  begin
    if FileExists(SaveDialog.FileName) then
    begin
      if IDYES = MessageBox(0,'Do you want to overwrite file ?','Exists File!',MB_YESNO) then wr := True
      else                                                                                    wr := False;
    end
    else wr := True;
  end;

  if (wr) then
  begin
    AssignFile(f,SaveDialog.FileName);
    {$I-}
    ReWrite(f,1);
    {$I+}
    if IOresult <> 0 then
    begin
      MessageBox(0,'Error currupted while writing file','error!',0);
      exit;
    end;

    for cntY:=0 to ImageInfo.Height-1 do
    for cntX:=0 to (ImageInfo.Width-1) do
    begin
      tmpRGB := TextureForm.TextureImage.Canvas.Pixels[cntX,cntY];

      if (ComboType.ItemIndex = DEF_DT_RGB888) then
      begin
        convertRGB2byte(tmpRGB,tmpR,tmpG,tmpB);
        blockwrite(f,tmpR,1);
        blockwrite(f,tmpG,1);
        blockwrite(f,tmpB,1);
      end
      else
      if (ComboType.ItemIndex = DEF_DT_ARGB8888 ) then
      begin
        convertRGB2byte(tmpRGB,tmpR,tmpG,tmpB);
        blockwrite(f,tmpR,1);
        blockwrite(f,tmpG,1);
        blockwrite(f,tmpB,1);
      end
      else
      if (ComboType.ItemIndex = DEF_DT_ARGB4444 ) then
      begin
      end
      else
      if (ComboType.ItemIndex = DEF_DT_RGB565 ) then
      begin
      end
      else
      begin
      end;
    end;

    Closefile(f);
  end;
end;

procedure TMForm.FormActivate(Sender: TObject);
begin
  if (FirstRun) then
  begin
    FirstRun := False;
    TextureForm.TextureImage.Canvas.FillRect(TextureForm.TextureImage.ClientRect);
  end;
end;

procedure TMForm.FormDestroy(Sender: TObject);
begin
  //SetLength(ConvertedData,0);
end;



end.
