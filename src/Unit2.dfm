object TextureForm: TTextureForm
  Left = 466
  Top = 103
  Width = 275
  Height = 293
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsSizeToolWin
  Caption = 'Rendered'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object TextureImage: TImage
    Left = 0
    Top = 0
    Width = 256
    Height = 256
  end
end
