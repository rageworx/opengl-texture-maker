unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TTextureForm = class(TForm)
    TextureImage: TImage;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetResizeFormToTexureImage;
  end;

var
  TextureForm: TTextureForm;

implementation

{$R *.dfm}

procedure TTextureForm.SetResizeFormToTexureImage;
begin
  TextureForm.ClientWidth  := TextureImage.Width;
  TextureForm.ClientHeight := TextureImage.Height;
end;

end.
