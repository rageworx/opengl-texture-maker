program OGLTM_proto;

{$DEFINE COMMERCIAL}

uses
  Forms,
  Unit1 in 'src\Unit1.pas' {MForm},
  Unit2 in 'src\Unit2.pas' {TextureForm},
  Unit3 in 'src\Unit3.pas' {LicenseForm};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMForm, MForm);
  Application.CreateForm(TTextureForm, TextureForm);
  Application.CreateForm(TLicenseForm, LicenseForm);
  Application.Run;
end.
